Dans le terminal flask run:

 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
127.0.0.1 - - [03/Dec/2023 19:08:36] "GET / HTTP/1.1" 200 -
127.0.0.1 - - [03/Dec/2023 19:08:37] "GET /private HTTP/1.1" 302 -
127.0.0.1 - - [03/Dec/2023 19:08:37] "GET /login?next=http://127.0.0.1:5000/private HTTP/1.1" 302 -
127.0.0.1 - - [03/Dec/2023 19:08:41] "GET /authorize?state=islVcAWM4hthS25BYfs8M61v0n3ypS&session_state=9bd35700-da45-419f-86fa-b6916f591c17&iss=http://localhost:8080/realms/practiceManager&code=5fb1d43b-90c5-4db5-bce3-ce0641f11d33.9bd35700-da45-419f-86fa-b6916f591c17.6b518899-ff4e-45f3-8bc7-02d60bc8a9e7 HTTP/1.1" 302 -
access_token=<eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJqaG9yUXJ4R0RMZ1BRUDNxcnBjNzEzUlFFdXE2SXNCSlRpdDdkd2dJbWxRIn0.eyJleHAiOjE3MDE2MjcyMjEsImlhdCI6MTcwMTYyNjkyMSwiYXV0aF90aW1lIjoxNzAxNjI2OTIxLCJqdGkiOiI0Y2ZhYTE3OC0zYTI0LTQ4YzMtOTBlZS1lZDc3MmE4NzBlOTMiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL3ByYWN0aWNlTWFuYWdlciIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI4NmY4ZWJmZi03MTk1LTRjZTUtODRiNS05MTJhMTQ5YmY2MDEiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJwYXRpZW50LUJhY2tFbmQiLCJub25jZSI6IkJzRWtyVVRFV0lWZXo4NU1DU2k5Iiwic2Vzc2lvbl9zdGF0ZSI6IjliZDM1NzAwLWRhNDUtNDE5Zi04NmZhLWI2OTE2ZjU5MWMxNyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiZGVmYXVsdC1yb2xlcy1wcmFjdGljZW1hbmFnZXIiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsInNpZCI6IjliZDM1NzAwLWRhNDUtNDE5Zi04NmZhLWI2OTE2ZjU5MWMxNyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4iLCJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSJ9.QM7ODNmIxHo3wboIHzjCjSULD6IC5RpnrUN0lPTCblIR5mbke_12fErydT44DhLGHeKSDSqNm1ARwGgU_7O763hGfWOjargLs4PItYXQDyvvyCKOwuY117DgayyhN_CW6ciHsbcczMrwBvZxjLl6dLl729vlxUS2b7h-FX6B9aaQmmsjXDqEa6IA_0U6Ru08WCByFPuFzmCPC65PdAO_RP8nb-zS38lLpLpKUo5a6opHW9NNkHd62BNrjfNIPSwh3QDxOclJKvoOG_haYzCz-EM7wKHzo59YgxWsnzfO1epP0J-xQ3IB4nhveXrN_WJGHkzNQDgXHRQNbbNUrY-PEg>
127.0.0.1 - - [03/Dec/2023 19:08:41] "GET /private HTTP/1.1" 200 -
127.0.0.1 - - [03/Dec/2023 19:10:12] "GET /hello HTTP/1.1" 401 -


####################################

Dans un autre terminal:

curl -X GET -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJqaG9yUXJ4R0RMZ1BRUDNxcnBjNzEzUlFFdXE2SXNCSlRpdDdkd2dJbWxRIn0.eyJleHAiOjE3MDE2MjcyMjEsImlhdCI6MTcwMTYyNjkyMSwiYXV0aF90aW1lIjoxNzAxNjI2OTIxLCJqdGkiOiI0Y2ZhYTE3OC0zYTI0LTQ4YzMtOTBlZS1lZDc3MmE4NzBlOTMiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL3ByYWN0aWNlTWFuYWdlciIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiI4NmY4ZWJmZi03MTk1LTRjZTUtODRiNS05MTJhMTQ5YmFkMDEiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJwYXRpZW50LUJhY2tFbmQiLCJub25jZSI6IkJzRWtyVVRFV0lWZXo4NU1DU2k5Iiwic2Vzc2lvbl9zdGF0ZSI6IjliZDM1NzAwLWRhNDUtNDE5Zi04NmZhLWI2OTE2ZjU5MWMxNyIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiZGVmYXVsdC1yb2xlcy1wcmFjdGljZW1hbmFnZXIiLCJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsInNpZCI6IjliZDM1NzAwLWRhNDUtNDE5Zi04NmZhLWI2OTE2ZjU5MWMxNyIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWRtaW4iLCJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSJ9.QM7ODNmIxHo3wboIHzjCjSULD6IC5RpnrUN0lPTCblIR5mbke_12fErydT44DhLGHeKSDSqNm1ARwGgU_7O763hGfWOjargLs4PItYXQDyvvyCKOwuY117DgayyhN_CW6ciHsbcczMrwBvZxjLl6dLl729vlxUS2b7h-FX6B9aaQmmsjXDqEa6IA_0U6Ru08WCByFPuFzmCPC65PdAO_RP8nb-zS38lLpLpKUo5a6opHW9NNkHd62BNrjfNIPSwh3QDxOclJKvoOG_haYzCz-EM7wKHzo59YgxWsnzfO1epP0J-xQ3IB4nhveXrN_WJGHkzNQDgXHRQNbbNUrY-PEg" http://localhost:5000/hello

####################################

Et j'obtiens l'erreur:

{"error": "invalid_token", "error_description": "The access token provided is expired, revoked, malformed, or invalid for other reasons."}

J'ai essayé de me deconnecter/me reconnecter, de re run flask mais j'ai toujours la même erreur