import unittest
from unittest.mock import Mock
from flask.testing import FlaskClient

from app import create_app
from patient.application.patient_service import PatientService
from patient.infrastructure.patient_entity import PatientEntity


class TestPatientResource(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.app.config['TESTING'] = True
        self.client: FlaskClient = self.app.test_client()

        self.mock_service = Mock(spec=PatientService)
        self.app.config['patient_service'] = self.mock_service

    ###############################################################

    def test_create_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.create_patient.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.post('/patients', json=patient_data)

        self.assertEqual(201, response.status_code)
        print(response.json)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.create_patient.assert_called_once()

    ###############################################################

    def test_get_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
           'social_security_number': 123456789012345
        }

        self.mock_service.get_patient_by_id.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.get('/patients/1')

        self.assertEqual(200, response.status_code)
        print(response.json)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.get_patient_by_id.assert_called_once()

    ###############################################################

    def test_get_patient_by_id_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
          'social_security_number': 123456789012345
        }

        self.mock_service.get_patient_by_id.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.get('/patients/id/1')

        self.assertEqual(200, response.status_code)
        print(response.json)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.get_patient_by_id.assert_called_once()

    ###############################################################

    def test_get_patient_by_ssn_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
        }

        self.mock_service.get_patient_by_ssn.return_value = PatientEntity(**{'id': 1, **patient_data})
        response = self.client.get('/patients/ssn/123456789012345')
        self.assertEqual(200, response.status_code)
        print(response.json)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.get_patient_by_ssn.assert_called_once()
    
    ###############################################################

    def test_delete_patient_endpoint(self):
        response = self.client.delete('/patients/1')

        self.assertEqual(204, response.status_code)
        self.mock_service.delete_patient.assert_called_once()
    
    ###############################################################

    def test_update_patient_endpoint(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': 123456789012345
        }

        self.mock_service.update_patient.return_value = PatientEntity(**{'id': 1, **patient_data})

        response = self.client.put('/patients/1', json=patient_data)

        self.assertEqual(200, response.status_code)
        print(response.json)
        self.assertEqual(response.json, {'id': 1, **patient_data})
        self.mock_service.update_patient.assert_called_once()


