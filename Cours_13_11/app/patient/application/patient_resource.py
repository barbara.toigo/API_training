from flask import request, jsonify, Blueprint
from flask import current_app as app

from patient.domain.patient import Patient

patient_bp = Blueprint('patient', __name__)


@patient_bp.route('/patients', methods=['POST'])
def create_patient():
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient_by_id(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_id(patient_id)

        if patient:
            return jsonify(patient.to_dict()), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_id(patient_id)

        if patient:
            return jsonify(patient.to_dict()), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/ssn/<int:ssn>', methods=['GET'])
def get_patient_by_ssn(ssn):
    try:
        patient_service = app.config['patient_service']
        patient = patient_service.get_patient_by_ssn(ssn)

        if patient:
            return jsonify(patient.to_dict()), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        patient_service.delete_patient(patient_id)

        return jsonify({'message': 'Patient deleted successfully'}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        updated_patient = patient_service.update_patient(patient_id, Patient(**patient_data))

        if updated_patient:
            return jsonify(updated_patient.to_dict()), 200
        else:
            return jsonify({'error': 'Patient not found'}), 404
    except Exception as e:
        return jsonify({'error': str(e)}), 400
