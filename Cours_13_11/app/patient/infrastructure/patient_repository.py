from patient.domain.patient import Patient
from patient.infrastructure.patient_entity import PatientEntity


class PatientRepository:
    def __init__(self):
        self.db = {}
        self.next_patient_id = 1

    def add_patient(self, patient: Patient) -> PatientEntity:
        # Generate a unique patient ID
        patient_id = self.next_patient_id
        self.next_patient_id += 1

        # Create a PatientEntity with ID
        patient_entity = PatientEntity(
            id=patient_id,
            first_name=patient.first_name,
            last_name=patient.last_name,
            date_of_birth=patient.date_of_birth,
            social_security_number=patient.social_security_number
        )

        # Store the patient entity in the database
        self.db[patient_id] = patient_entity

        # Return the created patient entity
        return patient_entity

    def get_patients(self):
        return list(self.db.values())
    
    def get_patient_by_id(self, patient_id):
        return self.db.get(patient_id)
    
    def get_patient_by_ssn(self, ssn):
        for patient in self.db.values():
            if patient.social_security_number == ssn:
                return patient
        return None
    
    def delete_patient(self, patient_id):
        return self.db.delete(patient_id)

    
    def update_patient(self, patient_id, patient: Patient) -> PatientEntity:
        patient_entity = self.get_patient_by_id(patient_id)
        if patient_entity:
            patient_entity.first_name = patient.first_name
            patient_entity.last_name = patient.last_name
            patient_entity.date_of_birth = patient.date_of_birth
            patient_entity.social_security_number = patient.social_security_number
            return patient_entity
        return None
    
    